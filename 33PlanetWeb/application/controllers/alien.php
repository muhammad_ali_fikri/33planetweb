<?php

require APPPATH . '/libraries/REST_Controller.php';

class alien extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
    }

    // show data alien
    function index_get() {
        $id_alien = $this->get('id_alien');
        if ($id_alien == '') {
            $alien = $this->db->get('alien')->result();
        } else {
            $this->db->where('id_alien', $id_alien);
            $alien = $this->db->get('alien')->result();
        }
        $this->response($alien, 200);
    }

    // insert new data to alien
    function index_post() {
        $data = array(
                    'id_alien'           => $this->post('id_alien'),
                    'nama_alien'          => $this->post('nama_alien'),
                    'followers_alien'    => $this->post('followers_alien'),
                    'alamat_alien'        => $this->post('alamat_alien'));
        $insert = $this->db->insert('alien', $data);
        if ($insert) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    // update data alien
    function index_put() {
        $id_alien = $this->put('id_alien');
        $data = array(
                    'id_alien'       => $this->put('id_alien'),
                    'nama_alien'      => $this->put('nama_alien'),
                    'followers_alien'=> $this->put('followers_alien'),
                    'alamat_alien'    => $this->put('alamat_alien'));
        $this->db->where('id_alien', $id_alien);
        $update = $this->db->update('alien', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    // delete alien
    function index_delete() {
        $id_alien = $this->delete('id_alien');
		
        $this->db->where('id_alien', $id_alien);
		
        $delete = $this->db->delete('alien');
		
        if ($delete) {
            $this->response(array('status' => 'success'), 201);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

}